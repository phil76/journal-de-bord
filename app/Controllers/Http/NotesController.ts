import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Note from 'App/Models/Note'
import Promo from 'App/Models/Promo'

export default class NotesController
{
    /**
     * Home page
     */
    public async index({ view, auth, request }: HttpContextContract)
    {
        const id: number = +request.param('id')

        if (await auth.check())
        {
            const promos = await auth.user?.related('promos').query()

            const granted: number[] | undefined = promos?.map(promo => promo.id)

            if (promos && granted && granted.includes(id)) {
                let notes: Note[] = []

                if (id){
                    notes = await Note.query().where('promo_id', id).preload("user") // .crossJoin('users', () => 'user_id')
                }

                notes?.reverse()

                return view.render('home', { promos: promos, notes: notes || [], id: id })
            }

            return view.render('home', { promos: promos, notes: [], id: undefined })
        }

    }

    public async store({ request, response, session, auth }: HttpContextContract)
    {
        const postSchema = schema.create({
            note: schema.string(),
            id: schema.number(),
        })

        const data = await request.validate({
            schema: postSchema,
            cacheKey: request.url(),
        })

        if (data.note.length && data.id)
        {
            const promo = await Promo.findOrFail(data.id)

            const userId = (await auth.authenticate()).id

            const note = await promo.related('notes').create({
                note: data.note,
                userId: userId
            })

            if (note)
            {
                session.flash('success', 'Note ajoutée avec succès')

                response.redirect().back()
            }
        }
    }

    public async update({ request, response, session, auth }: HttpContextContract)
    {   
        const postSchema = schema.create({
            editnote: schema.string(),
            id: schema.number(),
        })

        const data = await request.validate({
            schema: postSchema,
            cacheKey: request.url(),
        })

        if (data.editnote.length && data.id)
        {
            const note = await Note.findOrFail(data.id)

            if (note && note.userId === auth.user?.id)
            {
                note.note = data.editnote
                await note.save()
    
                session.flash('success', 'Note modifiée avec succès')
    
                response.redirect().back()
            }
            else
            {
                session.flash('error', 'Cette note ne vous appartient pas !')
    
                response.redirect().back()
            }
        }
    }
}

