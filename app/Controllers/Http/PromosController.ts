import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { schema } from '@ioc:Adonis/Core/Validator'
import Promo from 'App/Models/Promo'
import User from 'App/Models/User'




export default class PromosController
{
    /**
     * Affiche le formulaire
     */
    public async index({ response }: HttpContextContract)
    {
        return response.ctx?.view.render('add_promo')
    }

    /**
     * Ajouter une promo
     */
    public async store({ request, response, session, auth }: HttpContextContract)
    {
        const postSchema = schema.create({
            inputName: schema.string({ escape: true, trim: true }),
        })

        const data = await request.validate({
            schema: postSchema,
            cacheKey: request.url(),
        })

        if (data.inputName.length)
        {
            const team = await (await auth.authenticate()).related('promos').create({
                name: data.inputName
            })

            if (team)
            {
                session.flash('success', 'Promo crée avec succès')

                response.redirect().back()
            }
        }
    }


    /**
     * Formulaire pour ajouter un formateur a une promo
     */
    public async addFormerView({ view, auth }: HttpContextContract)
    {
        if (await auth.check())
        {
            const promos = await auth.user?.related('promos').query()

            const users = await User.all();

            if (promos)
            {
                promos.forEach(promo => promo.serialize())

                return view.render('add_formateur', { promos, users })
            }
        }
    }

    /**
     * Ajouter un formateur a une promo
     */
    public async addFormer({ request, response, session }: HttpContextContract)
    {
        const postSchema = schema.create({
            promo: schema.number(),
            former: schema.number()
        })

        const data = await request.validate({
            schema: postSchema,
            cacheKey: request.url(),
        })

        const user = await User.findOrFail(data.former).catch(() =>
        {
            this.handleError(session, response)
        })

        const promo = await Promo.findOrFail(data.promo).catch(() =>
        {
            this.handleError(session, response)
        })


        if (user && promo)
        {
            const result = await user.related('promos').create(promo).catch(() =>
            {
                session.flash('error', 'Une erreur est survenue')

                response.redirect().back()
            })

            if(result)
            {
                session.flash('success', 'Formateur ajouté avec succès')

                response.redirect().back()
            }
        }
    }

    private handleError(session, response)
    {
        session.flash('error', 'Une erreur est survenue')

        response.redirect().back()
    }
}
