/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer''
|
*/

import Route from '@ioc:Adonis/Core/Route'




// register
Route.on('register').render('register')
Route.post('register', 'AuthController.register')

// login
Route.on('login').render('login')
Route.post('login', 'AuthController.login')

// logout
Route.get('logout', 'AuthController.logout').middleware('auth')



Route.get('add-promo', 'PromosController.index').middleware('auth')
Route.post('add-promo', 'PromosController.store').middleware('auth')

Route.get('add-former', 'PromosController.addFormerView').middleware('auth')
Route.post('add-former', 'PromosController.addFormer').middleware('auth')

// Route.get('add-former', 'PromosController.addFormerView').middleware('auth')
Route.post('add-note', 'NotesController.store').middleware('auth')

Route.post('edit-note', 'NotesController.update').middleware('auth')

// notes
Route.get('/:id?', 'NotesController.index').middleware('auth')